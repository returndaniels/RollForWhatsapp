package com.multzone.rolagemWpp;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Vibrator;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.content.Intent;
import android.content.Context;
import android.widget.Button;
import android.widget.DigitalClock;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class MainActivity extends Activity {
    private Random gna = new Random();
    private DigitalClock timeBox;
    private int mod, counter, somaGeral;
    private String toCoder;
    private boolean modificador;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);

        SharedPreferences carregar = getSharedPreferences("data_save", MODE_PRIVATE);
        counter = carregar.getInt("sessao", 0);

        created();
    }
    private void created(){
        setContentView(R.layout.main);
        timeBox = (DigitalClock)findViewById(R.id.hora);
        mod = 0;

        EditText txtD4 = (EditText) findViewById(R.id.vezesd4);
        EditText txtD6 = (EditText) findViewById(R.id.vezesd6);
        EditText txtD8 = (EditText) findViewById(R.id.vezesd8);
        EditText txtD10 = (EditText) findViewById(R.id.vezesd10);
        EditText txtD12 = (EditText) findViewById(R.id.vezesd12);
        EditText txtD20 = (EditText) findViewById(R.id.vezesd20);
        EditText txtD100 = (EditText) findViewById(R.id.vezesd100);
        EditText txtDF = (EditText) findViewById(R.id.vezesdf);

        final ArrayList<EditText> objList = new ArrayList<EditText>();

        objList.add(txtD4);
        objList.add(txtD6);
        objList.add(txtD8);
        objList.add(txtD10);
        objList.add(txtD12);
        objList.add(txtD20);
        objList.add(txtD100);
        objList.add(txtDF);

        Button btnRoll = (Button)findViewById(R.id.btnRoll);
        btnRoll.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v)
            {
                int n;
                int dado=0;
                String type="";
                String message="";

                Vibrator rr = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                long milliseconds = 17;
                rr.vibrate(milliseconds);

                somaGeral = 0;

                for(int i=0;i<objList.size();++i) {
                    if (objList.get(i).getText().toString().equalsIgnoreCase(""))
                    {
                        objList.get(i).setText("0");
                    }
                    n = Byte.valueOf(objList.get(i).getText().toString());
                    switch (i){
                        case 0:
                            dado = 4;
                            type = "d4";
                            break;
                        case 1:
                            dado = 6;
                            type = "d6";
                            break;
                        case 2:
                            dado = 8;
                            type = "d8";
                            break;
                        case 3:
                            dado = 10;
                            type = "d10";
                            break;
                        case 4:
                            dado = 12;
                            type = "d12";
                            break;
                        case 5:
                            dado = 20;
                            type = "d20";
                            break;
                        case 6:
                            dado = 100;
                            type = "d100";
                            break;
                        case 7:
                            dado = 3;
                            type = "dF";
                            break;
                    }
                    if(n!=0){
                        counter ++;

                        SharedPreferences.Editor editor = getSharedPreferences("data_save", MODE_PRIVATE).edit();
                        editor.putInt("sessao",counter);
                        editor.apply();

                        message += main(n, dado, type) +"\n";
                    }
                }
               enviar(message);
            }});
        Button btnDec = (Button)findViewById(R.id.btnDEC);
        btnDec.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v)
            {
                Vibrator rr = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                long milliseconds = 17;
                rr.vibrate(milliseconds);

                decodificar();
            }});
        Button bInfo = (Button)findViewById(R.id.btnInfo);
        bInfo.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v)
            {
                Vibrator rr = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                long milliseconds = 17;
                rr.vibrate(milliseconds);

                about();
            }});
    }
    private String main(int n, int dado, String type){
        EditText txtMod = (EditText) findViewById(R.id.mod);
        ArrayList<Integer> lista = listCreator(n, dado, type);
        String hora = timeBox.getText().toString();

        if (txtMod.getText().toString().equalsIgnoreCase(""))
        {
            txtMod.setText("0");
        }
        mod = Integer.valueOf(txtMod.getText().toString());


        String texto = "\n*Rolagem:* "+hora+" - "+n+type;
        toCoder = "\nRolagem: "+hora+" - "+n+type;
        /**
            if (mod > 0) {
                texto += " +" + mod;
                toCoder += " +" + mod;
            } else if (mod < 0) {
                texto += " " + mod;
                toCoder += " " + mod;
            }
         **/
        int soma = lista.get(0);
        String value = "";

        if(type=="dF"){
            switch (lista.get(0)){
                case -1:
                    value = "{-}";
                    break;
                case 0:
                    value = "{0}";
                    break;
                case 1:
                    value = "{-}";
                    break;
            }
        }else {
            value=""+lista.get(0);
        }

        texto += "\n_*["+value+"*";
        toCoder += "\n["+value;

        for(int i = 1; i < lista.size(); ++i){
            soma += lista.get(i);
            if(type=="dF"){
                switch (lista.get(i)){
                    case -1:
                        texto += ", {-}";
                        toCoder += ", {-}";
                        break;
                    case 0:
                        texto += ", {0}";
                        toCoder += ", {0}";
                        break;
                    case 1:
                        texto += ", {+}";
                        toCoder += ", {+}";
                        break;
                }
            }else {
                texto += ", "+lista.get(i);
                toCoder += ", "+lista.get(i);
            }
        }
        texto += " *]*_\n\n```Total:``` ";
        toCoder += " ]\n\nTotal: ";

           if(type=="dF") {soma += mod;}
            somaGeral += soma;

        if(type=="dF"){
            if(soma < 0){
                texto += "{-}";
                toCoder += "{-}";
            }
            else if(soma == 0){
                texto += "{0}";
                toCoder += "{0}";
            }
            else if(soma > 0){
                texto += "{+}";
                toCoder += "{+}";
            }
        }else {
            texto += soma;
            toCoder += soma;
        }
        toCoder += "\nID: "+counter;

        return texto;
    }
    public void about() {
        setContentView(R.layout.about);

        Button home = (Button)findViewById(R.id.btnHome);
        home.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v)
            {

                Vibrator rr = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                long milliseconds = 17;
                rr.vibrate(milliseconds);

                created();
            }});
    }
    public void onRadioButtonClicked(View view) {
        boolean checked = ((RadioButton) view).isChecked();
        switch(view.getId()) {
            case R.id.mais:
                if (checked) {
                    mod *= 1;
                    modificador = true;
                }else{
                    modificador = false;
                }
                break;
            case R.id.menos:
                if (checked) {
                    mod *= -1;
                    modificador = true;
                }else{

                    modificador = false;
                }
                break;
        }
    }
    private void enviar(String msg){

        int result = somaGeral+mod;
        if (modificador) {
            msg += "\n*Soma Geral:* " + somaGeral + "+" + mod + "=" + result + "\n\nCodigo: " + codificar(toCoder);
        }else{
            msg += "\n*Soma Geral:* " + somaGeral + "\n\nCodigo: " + codificar(toCoder);
        }
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.setType("text/plain");
        sendIntent.putExtra(Intent.EXTRA_TEXT, msg);
        startActivity(sendIntent.setPackage("com.whatsapp"));
    }
    private ArrayList<Integer> listCreator(int n, int dado, String type){
        ArrayList<Integer> rolled = new ArrayList<>();

        for(int i=0; i<n; ++i){
            rolled.add(roll(dado, type));
        }
        Collections.sort(rolled, Collections.reverseOrder());

        return rolled;
    }
    private int roll(int dado, String type){
        int value = gna.nextInt(dado)+1;
        if(type == "dF"){value -= 2;}

        return value;
    }
    private String codificar(String arg){
        String codigo = null;

        try
        {
            Encripta encripta = new Encripta();
            codigo =  encripta.encrypt(arg);
            Log.v("Criptografia", codigo);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return codigo;
    }
    private void decodificar(){

        setContentView(R.layout.decode);
        TextView rolei = (TextView) findViewById(R.id.tvRolei);

        findViewById(R.id.btnDecode).setVisibility(View.VISIBLE);
        findViewById(R.id.btnClear).setVisibility(View.GONE);

        rolei.setText("Você fez " + counter + " rolagens.");

        Button zerar = (Button) findViewById(R.id.btnZerar);
        zerar.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v)
            {
                Vibrator rr = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                long milliseconds = 17;
                rr.vibrate(milliseconds);

                counter = 0;

                SharedPreferences.Editor editor = getSharedPreferences("data_save", MODE_PRIVATE).edit();
                editor.putInt("sessao",counter);
                editor.apply();
                decodificar();
            }});
        Button btnDcode = (Button)findViewById(R.id.btnDecode);
        btnDcode.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v)
            {
                EditText tCode    = (EditText) findViewById(R.id.txtCode);
                TextView tview   = (TextView) findViewById(R.id.tv);

                Vibrator rr = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                long milliseconds = 17;
                rr.vibrate(milliseconds);

                try
                {
                    Encripta encripta = new Encripta();
                    // encripta.getIV();
                    String codigoDecifrado =  encripta.decrypt(tCode.getText().toString());

                    Log.v("Msg a ser Decifrada", codigoDecifrado);

                    Log.v("Decifrado", codigoDecifrado);
                    tCode.setText(null);
                    tview.setText(codigoDecifrado);
                    try
                    {
                        findViewById(R.id.btnDecode).setVisibility(View.GONE);
                        findViewById(R.id.btnClear).setVisibility(View.VISIBLE);

                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }

                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }});
        Button Clear = (Button)findViewById(R.id.btnClear);
        Clear.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v)
            {
                TextView tv = (TextView) findViewById(R.id.tv);

                Vibrator rr = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                long milliseconds = 17;
                rr.vibrate(milliseconds);

                tv.setText(null);

                decodificar();
            }});
        Button btnVoltar = (Button)findViewById(R.id.btnBack);
        btnVoltar.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v)
            {

                Vibrator rr = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
                long milliseconds = 17;
                rr.vibrate(milliseconds);

                created();
            }});
    }
}